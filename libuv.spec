%bcond_without scl

%if %{with scl}
%global scl isc-bind
%endif

# Work around an SCL build issue on Fedora 33+
# (https://bugzilla.redhat.com/show_bug.cgi?id=1898587)
%if 0%{?fedora} >= 33
%global __python %{__python3}
%endif

%{?scl:%scl_package libuv}
%{!?scl:%global pkg_name %{name}}

# Run the tests by default
%bcond_without tests

Name:           %{?scl_prefix}libuv
Epoch:          1
Version:        1.49.2
Release:        %autorelease
Summary:        Platform layer for node.js

# Code is MIT
# Documentation is CC-BY-4.0
# src/inet.c is ISC
# include/uv/tree.h is BSD-2-Clause
License:        MIT AND CC-BY-4.0 AND ISC AND BSD-2-Clause
URL:            https://libuv.org/
Source0:        https://dist.libuv.org/dist/v%{version}/libuv-v%{version}.tar.gz

BuildRequires:  autoconf automake libtool
Source3:        libuv.abignore
BuildRequires:  gcc
BuildRequires:  make

%{?scl:BuildRequires: %{scl}-build}
%{?scl:Requires: %scl_runtime}

%description
libuv is a new platform layer for Node. Its purpose is to abstract IOCP on
Windows and libev on Unix systems. We intend to eventually contain all platform
differences in this library.

%package devel
Summary:        Development libraries for libuv
Requires:       %{name}%{?_isa} = %{epoch}:%{version}-%{release}
Requires:       %{name}-static%{?_isa} = %{epoch}:%{version}-%{release}


%description devel
Development libraries for libuv

%package static
Summary:        Platform layer for node.js - static library
Requires:       %{name}-devel%{?_isa} = %{epoch}:%{version}-%{release}

%description static
Static library (.a) version of libuv.


%prep
%autosetup -n %{pkg_name}-v%{version} -p1

%build
./autogen.sh
%configure --disable-silent-rules
%make_build

%install
%make_install
rm -f %{buildroot}%{_libdir}/libuv.la
install -Dm0644 -t %{buildroot}%{_libdir}/libuv/ %{SOURCE3}


%check
%if %{with tests}
%make_build check
%endif

%ldconfig_scriptlets

%files
%doc README.md AUTHORS CONTRIBUTING.md MAINTAINERS.md SUPPORTED_PLATFORMS.md
%doc ChangeLog
%license LICENSE LICENSE-docs LICENSE-extra
%{_libdir}/%{pkg_name}.so.1
%{_libdir}/%{pkg_name}.so.1.*
%dir %{_libdir}/%{pkg_name}
%{_libdir}/%{pkg_name}/libuv.abignore

%files devel
%{_includedir}/uv.h
%dir %{_includedir}/uv
%{_includedir}/uv/*.h
%{_libdir}/%{pkg_name}.so
%{_libdir}/pkgconfig/%{pkg_name}.pc

%files static
%{_libdir}/%{pkg_name}.a

%changelog
%autochangelog
